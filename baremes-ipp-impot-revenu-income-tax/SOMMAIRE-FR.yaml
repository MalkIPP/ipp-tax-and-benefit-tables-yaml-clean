Titre: 'Barèmes IPP : Impôt sur le revenu'
Description: Ce document présente l'ensemble de la législation permettant le calcul de l'impôt sur le revenu (IR). Il s'agit
  des barèmes bruts de la législation utilisés dans le micro-simulateur de l'IPP, TAXIPP. Les sources législatives (texte
  de loi, numéro du décret ou arrêté) ainsi que la date de publication au Journal Officiel de la République Française (JORF)
  sont indiquées dans la mesure du possible. La première ligne du fichier (masquée) indique le nom des paramètres dans TAXIPP.
  Nous présentons d'abord les mécanismes d'assiette (revenus concernés, déductions, etc.) puis les paramètres du barèmes (tranches
  et taux) avec les effets de parts (quotient familial). Ensuite, nous présentons les diverses réductions sur l'IR puis les
  crédits d'impôts. Les contributions exceptionnelles sont regroupées à la fin.
Sommaire:
  Annexes:
    bareme-ir: Seuils et taux d'imposition au titre du barème de l'IR (1945 - 2015)
    bareme-igr: Barème de l'impôt général sur le revenu IGR (1914 - 1944)
  I. Calcul des revenus imposables:
    deductions: Déductions salaires, pensions (1978 - 2015)
    rcm: Revenu des valeurs et capitaux mobiliers (1980 - 2015)
    micro: Régimes micro-entreprise et microfoncier (1980 - 2015)
    charg-deduc: Charges déductibles du revenu brut global (1975 - 2015)
    abat-rni: Abattement pour revenu net imposable ( 1978 - 2015)
    exo-ir: Exonération d'IR (1978 - 2015)
  II. Calcul de l'impôt sur le revenu:
    plaf-qf: Plafonnement du quotient familial et décote (1978 - 2015)
    pv: Imposition des plus-values (1979 - 2015)
  III. Calcul des réductions d'impôts:
    dons: Dons (1977 - 2015)
    deduc-sal: Cotisations syndicales (1989 - 2015)
    sofipeche: Souscription au capital de SOFIPECHE (1999 - 2015)
    sal-dom: Emploi d'un salarié à domicile (1992 - 2015)
    prest-compen: Prestation compensatoire (2001 - 2015)
    fcp: Souscription de parts de fonds communs de placement dans l'innovation (1998 - 2015)
    sofica: Souscription au capital de SOFICA (1986 - 2015)
    pme: Souscription au capital des PME (1993 - 2015)
    foret: Investissement et travaux forestiers (2002 - 2015)
    enfscol: Réductions pour enfants scolarisés (1993 - 2015)
    heberg-sante: Dépenses d’accueil dans un établissement pour personnes dépendantes (1990 - 2015)
    habitat-princ: Dépenses de gros travaux et intérêts d'emprunt et ravalement (1980 - 2015)
    codev: Sommes versées sur un compte épargne codéveloppement (2010 - 2012)
    divers: Autres réductions d'impôts (1979 - 2015)
  IV. Calcul des crédits d'impôts:
    gardenf: Frais de garde d'enfants (1979 - 2015)
    ppe: Prime pour l'emploi (2001 - 2015)
    plaf-nich: Plafonnement global des niches (2009 - 2015)
  V. Contributions exceptionnelles:
    taxe-hr: Taxe exceptionnelle sur les hauts revenus (2012-2015)
    majo-excep: Majorations exceptionnelles (1947-1981)
Notes: |-
  Citer cette source :
  Barèmes IPP: Impôt sur le revenu, Institut des politiques publiques, avril 2015.

  Auteurs :
  Mathias André, Jonathan Goupille,  Malka Guillot, Thomas Piketty et Marianne Tenand

  Contacts :
  Antoine Bozio, antoine.bozio@ipp.eu
  Malka Guillot, malka.guillot@ipp.eu
Données initiales:
  Producteur: Institut des politiques publiques
  Format: XLS
  URL: http://www.ipp.eu/outils/baremes-ipp/
Convertisseur:
  URL: https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-converters
Données générées:
  Format: YAML
  URL: https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-raw
Licence: Licence ouverte <http://www.etalab.gouv.fr/licence-ouverte-open-licence>
